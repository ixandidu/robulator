# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'robulator/version'

Gem::Specification.new do |spec|
  spec.name          = "robulator"
  spec.version       = Robulator::VERSION
  spec.authors       = ["Ikhsan Maulana"]
  spec.email         = ["ixandidu@gmail.com"]

  spec.summary       = %q{Toy Robot Simulation.}
  spec.description   = %q{Robulator is a simulation of a toy robot moving on a square tabletop.}
  spec.homepage      = "https://bitbucket.org/ixandidu/robulator"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.8"
end
