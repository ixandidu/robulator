require 'test_helper'

describe ::Robulator::Tabletop do
  parallelize_me!

  tabletop_size = 5

  let(:tabletop) { ::Robulator::Tabletop.new(tabletop_size) }

  (0...tabletop_size).each do |x_axis|
    it { tabletop.placeable_spot?(x_axis, tabletop_size).wont_equal true }
    it { tabletop.placeable_spot?(tabletop_size, x_axis).wont_equal true }

    (0...tabletop_size).each do |y_axis|
      it { tabletop.placeable_spot?(x_axis, y_axis).must_equal true }
    end
  end
end
