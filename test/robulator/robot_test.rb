require 'test_helper'

describe ::Robulator::Robot do
  parallelize_me!

  let(:robot)             { ::Robulator::Robot.new }
  let(:initial_placement) { {x: 0, y: 0} }

  describe '#place' do
    describe 'given invalid position' do
      before { robot.place('x', 0, :north) }
      it     { robot.wont_be :placed? }
    end

    describe 'when the valid position is a string' do
      before { robot.place('-1', '0', :north) }
      it     { robot.must_be :placed? }
    end

    describe 'given invalid direction' do
      before { robot.place(0, 0, :up) }
      it     { robot.wont_be :placed? }
    end

    describe 'when the valid direction is a string' do
      before { robot.place(0, 0, 'north') }
      it     { robot.must_be :placed? }
    end
  end

  [
    [:north, {y: 1},  :east,  :west],
    [:south, {y: -1}, :west,  :east],
    [:east,  {x: 1},  :south, :north],
    [:west,  {x: -1}, :north, :south]
  ].each do |initial_direction, move, right, left|
    describe "placed facing #{initial_direction}" do
      let(:next_position_from_initial) { initial_placement.update(move) }

      before do
        robot.place(
          initial_placement[:x],
          initial_placement[:y],
          initial_direction
        )
      end

      it { robot.must_be :placed? }
      it { robot.next_position.must_equal next_position_from_initial }

      describe '#move' do
        before { robot.move }
        it     { robot.position.must_equal      initial_placement.merge(move) }
        it     { robot.next_position.wont_equal next_position_from_initial }
      end

      describe '#turn' do
        describe 'to the right' do
          before { robot.turn :right }
          it     { robot.direction.must_equal     right }
          it     { robot.next_position.wont_equal next_position_from_initial }
        end

        describe 'to the left' do
          before { robot.turn :left }
          it     { robot.direction.must_equal     left }
          it     { robot.next_position.wont_equal next_position_from_initial }
        end

        describe 'ignore invalid turn' do
          %i[up down back].each do |invalid_relative_direction|
            before { robot.turn invalid_relative_direction }
            it     { robot.direction.must_equal initial_direction }
          end
        end
      end
    end
  end

  describe 'unplaced robot' do
    it { robot.wont_be :placed? }
    it { robot.next_position.must_be_nil }

    it 'will not move' do
      robot.move
      robot.position.must_be_nil
    end

    it 'will not turn' do
      robot.turn :left
      robot.direction.must_be_nil

      robot.turn :right
      robot.direction.must_be_nil
    end
  end
end
