require 'test_helper'

describe ::Robulator::Simulator do
  let(:robot)     { Minitest::Mock.new }
  let(:simulator) { ::Robulator::Simulator.new(robot) }

  describe 'the PLACE command' do
    it 'placed the toy robot on the tabletop' do
      robot.expect :place, true, ['0', '0', 'NORTH']

      simulator.stub :safe_place?, true do
        simulator.perform 'PLACE 0,0,NORTH'
        robot.verify
      end
    end

    it 'ignore the command when the arguments is invalid' do
      [
        'PLACE',
        'PLACE 0',
        'PLACE 0,0',
        'PLACE 0,0,NORTH,WEST'
      ].each do |place_command_with_invalid_arguments|
        simulator.perform place_command_with_invalid_arguments
        robot.verify
      end
    end

    it 'ignore the command that would cause the robot to fall' do
      simulator.stub :safe_place?, false do
        simulator.perform 'PLACE 0,0,NORTH'
        robot.verify
      end
    end
  end

  describe 'the MOVE command' do
    it 'move the robot forward' do
      robot.expect :move, true

      simulator.stub :safe_to_move?, true do
        simulator.perform 'MOVE'
        robot.verify
      end
    end

    it 'ignore the command that would cause the robot to fall' do
      simulator.stub :safe_to_move?, false do
        simulator.perform 'MOVE'
        robot.verify
      end
    end
  end

  describe 'the RIGHT command' do
    it 'turn the robot to the right' do
      robot.expect :turn, true, [:right]

      simulator.perform 'RIGHT'
      robot.verify
    end
  end

  describe 'the LEFT command' do
    it 'turn the robot to the left' do
      robot.expect :turn, true, [:left]

      simulator.perform 'LEFT'
      robot.verify
    end
  end

  describe 'the REPORT command' do
    it 'print a raw position and direction of the robot' do
      robot.expect :placed?,   true
      robot.expect :position,  {x: 500, y: 50}
      robot.expect :direction, :west

      ->{ simulator.perform('REPORT') }.must_output "500,50,WEST\n"
    end

    it 'ignore the command when the robot is not placed' do
      robot.expect :placed?, false

      simulator.perform 'REPORT'
      robot.verify
    end
  end

  it 'ignore invalid command' do
    simulator.perform('JUMP')
    robot.verify
  end
end
