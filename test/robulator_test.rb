require 'test_helper'

class RobulatorTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Robulator::VERSION
  end

  def test_subsequence_of_inputs
    simulator = ::Robulator::Simulator.new

    {
      "0,1,NORTH\n" => [
        'PLACE 0,0,NORTH',
        'MOVE'
      ],
      "0,0,WEST\n" => [
        'PLACE 0 ,0 ,NORTH',
        'LEFT'
      ],
      "3,3,NORTH\n" => [
        'PLACE 1, 2, EAST',
        'MOVE',
        'MOVE',
        'LEFT',
        'MOVE'
      ]
    }.each do |expected_report_output, movement_commands|
      movement_commands.each do |movement_command|
        simulator.perform movement_command
      end

      assert_output expected_report_output do
        simulator.perform 'REPORT'
      end
    end
  end
end
