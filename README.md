# Robulator
Robulator is a simulation of a toy robot moving on a square tabletop,
of dimensions 5 units x 5 units.

             y
             ^
             |     NORTH
             |
           4 |---+---+---+---+
             |   |   |   |   |
           3 |---+---+---+---+
             |   |   |   |   |
    WEST   2 |---+---+---+---+   EAST
             |   |   |   |   |
           1 |---+---+---+---+
             |   |   |   |   |
             |---|---|---|---|----> x
           0     1   2   3   4

                   SOUTH


## Installation
Install it with:

    $ gem install robulator


## Usage
Start the toy robot simulator with:

    $ robulator

Robulator accept commands in the following form:

    PLACE X,Y,F
    MOVE
    LEFT
    RIGHT
    REPORT

### PLACE
Put the toy robot on the table in position `X`,`Y` and facing `NORTH`, `SOUTH`,
`EAST` or `WEST`.

  * The origin (0,0) is the `SOUTH WEST` most corner.

  * The first valid command to the robot is a `PLACE` command, after that,
    any sequence of commands may be issued, in any order, including another
    `PLACE` command. Robulator discard all commands in the sequence until a
    valid `PLACE` command has been executed.

### MOVE
Move the toy robot one unit forward in the direction it is currently facing.

### LEFT and RIGHT
Rotate the robot 90 degrees in the specified direction without changing the
position of the robot.

### REPORT
Announce the `X`,`Y` and the direction of the robot to standard output (e.g.
`2,4,SOUTH`).


## Example Input and Output

    >> PLACE 0,0,NORTH
    >> MOVE
    >> REPORT
    0,1,NORTH

    >> PLACE 0,0,NORTH
    >> LEFT
    >> REPORT
    0,0,WEST

    >> PLACE 1,2,EAST
    >> MOVE
    >> MOVE
    >> LEFT
    >> MOVE
    >> REPORT
    3,3,NORTH


## Conditions and Characteristics

  * There are no other obstructions on the table surface.

  * The robot is free to roam around the surface of the table but it ignore
    commands that would cause the robot to fall, this also includes the initial
    placement of the toy robot, however further valid commands will be accepted.

  * A robot that is not on the table will ignore the `MOVE`, `LEFT`, `RIGHT` and
    `REPORT` commands.

  * Robulator does not provide any graphical output showing the movement of the
    toy robot.


## Development
After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment. Run `bundle exec robulator` to use the gem in this directory, ignoring other installed copies of this gem.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).


## Contributing
Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/ixandidu/robulator. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](contributor-covenant.org) code of conduct.


## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
