require 'forwardable'

module Robulator
  class Simulator
    extend ::Forwardable

    def_delegators :@robot,
                   :place,
                   :placed?,
                   :move,
                   :turn,
                   :position,
                   :direction,
                   :next_position

    def_delegators :@tabletop,
                   :placeable_spot?

    PERFORMABLE_COMMANDS = %w[PLACE MOVE RIGHT LEFT REPORT]

    def initialize(robot=Robot.new, tabletop=Tabletop.new(5))
      @robot    = robot
      @tabletop = tabletop
    end

    def perform(command)
      command, *args = command.split(/\s|,/).reject(&:empty?)

      case command
      when 'PLACE';         place(*args) if args.size == 3 && safe_place?(*args)
      when 'MOVE';          move         if safe_to_move?
      when 'RIGHT', 'LEFT'; turn(command.downcase.to_sym)
      when 'REPORT';        send(command.downcase)
      end
    end

    private
      def report
        return unless placed?
        puts "#{position.values.join(',')},#{direction.upcase}"
      end

      def safe_place?(x, y, _direction)
        placeable_spot?(x, y)
      end

      def safe_to_move?
        placeable_spot?(next_position[:x], next_position[:y])
      end
  end
end
