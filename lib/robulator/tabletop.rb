module Robulator
  class Tabletop
    def initialize(size)
      size             = Integer(size)
      @placeable_spots = {x: 0...size, y: 0...size}
    end

    def placeable_spot?(x, y)
      @placeable_spots[:x].include?(Integer(x)) &&
      @placeable_spots[:y].include?(Integer(y))
    rescue ArgumentError
      false
    end
  end
end
