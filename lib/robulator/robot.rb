module Robulator
  class Robot
    ORIENTATIONS = {
      north: {move: [:y, 1],  turns: {right: :east,  left: :west}},
      south: {move: [:y, -1], turns: {right: :west,  left: :east}},
      east:  {move: [:x, 1],  turns: {right: :south, left: :north}},
      west:  {move: [:x, -1], turns: {right: :north, left: :south}}
    }.freeze
    VALID_TURN = %i[right left].freeze

    attr_reader :position, :direction

    def place(x, y, direction)
      direction = direction.to_sym.downcase unless direction.is_a?(Symbol)
      return unless ORIENTATIONS.keys.include?(direction)

      @position  = {x: Integer(x), y: Integer(y)}
      @direction = direction
    rescue ArgumentError
    end

    def placed?
      !!(direction && position)
    end

    def next_position
      return unless placed?
      axis, value = *ORIENTATIONS[direction][:move]
      position.merge({axis => position[axis] + value})
    end

    def move
      @position = next_position if next_position
    end

    def turn(relative_direction)
      return if !placed? || !VALID_TURN.include?(relative_direction)
      @direction = ORIENTATIONS[direction][:turns][relative_direction]
    end
  end
end
